# F18 UFC

## Description
This project was to scratch build a F18 up front controller (UFC) for use with DCS World. It integrates with DCS BIOS which provides the interface between a serial or UDP/IP device for exchanging the state of the aircraft in the game and the hardware controller (switches, indicators, servos, etc).

## Hardware
The hardware folder contains the Altium and Solidworks design files that describe the circuit board(s) and enclosure.

## Firmware
The firmware folder contains the software for the STM32 micro processor(s) uses in the project. These devices perform the translation between hardware states and game aircraft states.

## Usage
* Get the PCB printed
* Populate it against the BOM
* Connect the board to a powered USB HUB to the machine with the firmware checked out
* Connect the STM32 LinkV2 programmer to the programming port of the PCB
* Open firmware (STM32CubeIDE or VSCode STM32 extension with tools installed)
* Compile code
  * STM32CubeIDE - project->build
  * VSCode STM32 extension - Click the extension icon and hit build
* Flash device
  * STM32CubeIDE - project->upload
  * VSCode STM32 extension - Click the extension icon and hit flash
* plug PCB into DCS World machine via powered USB Hub
* Run DCSBIOS with F18 addon
* Run DCS
