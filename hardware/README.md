# F18-UFC Hardware

## PCB Design
### MCU
MCU selected was based on the limited availability of MCUs at Digikey - initially a STM32L031C8 was selected since the device was a LQFP-48 and hand solderable. Future updates will expand to a MCU with more RAM and GPIO. MCU would be programmed through SWD.

The MCU was somewhat underpowered for the application (once I learned more about the DCSBIOS interface). DCSBIOS would ideally be given 64k of RAM to itself. Ideally as well, the core speed of the MCU should be above the HSI of 16MHz the STM32L031 provides.

### Switches
The board used some Cherry MX Brown switches I had already, which helps explain it's overall size (it is much larger than required). The trick with the displays was the physical size and number of characters. The UFC needed a scratchpad with a minimum of 12 digits (which led to a 16 digit display), and each option display required 5 digits (which led to 8 char displays).

Switches are wired in a matrix configuration with each column being switched by the micro from 3.3V to 0V. It then goes through a 1Kohm resistor (current limit encase the micro is configured wrong), the micro is expected to provide a 100K pull down resistor on each row pin.

There is 24 switches in total, with a single position in the matrix not populated.

### Displays
7 segment displays would not have met the need as it needs 11 segment displays - they are also far too expensive relative to LCDs. If realism was wanted, the original displays are even harder to source (VFDs). LCDs can be on a parallel bus, so each of the 6 LCDs share D0-D7, RW, and R/S pins. It's only the enable pins that are broken out to the MCU.

Displays are controlled via software as to which are 8 digit and which are 16 as there is no hardware feedback that indicates number of digits.

Contrast is controlled via SMT pots attached to the back of the PCB (1 per display).

### DCSBIOS Interface
An IP stack would have been crazy for this level of MCU (might as well used a RPi then), so the connection is via serial UART - in this case a FTDI 3.3V UART to USB converter (FT231X). This provides the required 250,000 buad (8N1).

The USB interface is impedance matched to 100ohms (standard FR4 2 layer board substrate) and length matched for high speed switching. The traces are also very short, so likely didn't matter in the end.

## Enclosure Design
The design is pretty straight forward, it is a front and rear shell for the PCB, connected via M3 screws. See the model.sldasm to see the setup. A table mount was designed for my custom rig, but likely won't work for others.

### Access
Both the USB and programming port are exposed out the back plate. The programming port has a shroud so shouldn't be a big risk if it isn't normally connected. The contrast adjustments are not exposed through the back port, and should adjusted prior to assembly.

### Keycaps
Keycaps were designed for the Cherry MX Brown switches (I'm too cheap to buy custom keycaps). The best printing success I had (Prusa MK3S+) was to print 2 layers of clear PETG, then 2 layers of black PLA, then the rest in white PLA (which is what the gcode will do). When printed on the pebbled Prusa print sheets it makes a nice matte finish with clear enough text (printing with 0.4mm nozzle).
