# F18-UFC Firmware

## Build Env
Originally the project was an output of the STM32CubeMX software, designed for use with STM32CubeIDE. I really do not prefer the CubeIDE as it is eclipse based, and boo on eclipse. Within STM32CubeMX project settings, you can select the output type of 'Makefile' which allows you to use arm-none-eabi + make + openocd directly (which means any editor can be used).

The VSCode STM32 extension will use the makefile output directly and create a handy set of buttons to build, flash and debug your software.

## DCS BIOS Interface
DCS BIOS wants a serial port with 250,000 buad 8N1. Once connected it has two protocols for communication.

### MCU to DCSBIOS Protocol

This interface is used to give feedback to DCS based on the real hardware, ie switch states, rotary encoder positions, etc. The data is simple ASCII lines ended with a line feed. The format is:

```
<control name> <value>\r
```

Once you have installed DCS BIOS on your machine, it will host a webpage via port 5010. Part of that page is the command reference where it will indicate what the control name should be - as well as it's valid values. Remember that this was designed to work with Arduino, so the example code will not work directly.

Keys are updated by edge, ie as a key is pressed and it's state moves from off (released) to on (pressed) - a message is sent to DCSBIOS, then when the key is released, another message is sent. ie

```
UFC_1 1\r
UFC_1 0\r
```

This would press down the UFC keypad button '1' then release it. You do not need to send constant updates that the key is still pressed (or released), just one message on each edge.

### DCSBIOS to MCU Protocol

This is a binary protocol that sends data in little-endian byte ordering (the ARM processors are little-endian as well, so no issuer there). This interface generally tells the MCU about what to write on displays, move servos, turn on/off LEDs etc. For the purpose of this page, we'll call all output as for indicators, but it can be many types - but this saves me from listing them each time. This interface isn't as well described in the partially documented developer page from DCSBIOS. The way it functions is by record, each record contains:

```c
typedef struct {
    uint16_t address;
    uint16_t length;
    uint8_t data[];
} record;
```

The address is not a address necessarily to a indictor, it is the address within a memory map where the entire aircraft status is. Since the address is 16 bits, it means the entire aircraft's indicators are within 65535 bytes. Each record updates a portion of that memory map.

From recording live traffic from DCSBIOS, updates don't come in order. Essentially DCSBIOS determines what to update and how to boundary it. ie when updating the option displays on the UFC, it often sends data that would encompass several displays.

The firmware captures a record, then updates a modified memory map. Since the MCU selected only has 8k of RAM (and writing to Flash would be way too slow), only 42 bytes of the 65535 bytes actually represented the displays I put on the UFC. So once a record comes in, the address is used to update contents within the modified memory map. Then on a given cadence, the LCDs are updated based on whatever the memory map currently has. In order to limit flicker, if the contents of the LCD haven't changed, no update is sent to the LCD.

Each burst of records is preceded with a synchronisation sequence (0x55 0x55 0x55 0x55). This means at any time the serial reception software starts, it first looks for a sync sequence to know the next two bytes are the address of the first record. According to the documentation (but unconfirmed), there is 30 bursts a second.

### Serial Handlilng

Since the MCU is only operating at 16MHz, and the buad rate is 250kbaud. There is only 640 instructions between bytes being received. Which is a touch close if using a non-interrupt based approach to receiving the data.

That is why there is a FIFO circular buffer attached to a interrupt driven serial port.

A FIFO is created for tx and rx for the UART. On a received byte - the rx FIFO is written to, when we want to send a byte the TX FIFO is written to. The interrupts are manipulated to ensure that when bytes exist in the TX buffer, the UART will continue to send them as the transmission buffer empties. As bytes are received, the RX FIFO has the bytes pushed into it.

After the FIFO, is the data processing component (the DCSBIOS record decoder).

The FIFO + ISR pattern allows us to separate the data collection and data processing components (if needed).

#### DCSBIOS Record decoder

The decoder is a state machine that looks for 4 states:

1. Waiting for a sync sequence (4)
1. Reading address bytes (2)
1. Reading length bytes (2)
1. Reading data bytes (length)

As we are just updating a memory map, as we read the data bytes, they are just immediately updated in the memory map based on the address and current number of bytes read towards the record's length. So the record struct implemented in the code, does not contain a data portion.

