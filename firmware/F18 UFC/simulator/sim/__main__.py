import argparse
import struct
import sys
import serial
from time import sleep, time
import threading
import queue

class TXSerial(threading.Thread):
    def __init__(self, con):
        threading.Thread.__init__(self)
        self.con = con
        self.daemon = True
        self.active = False

        self.buff = queue.Queue()
    
    def run(self):
        self.active = True

        try:
            while self.active:
                while self.buff.empty() is False:
                    data = self.buff.get()
                    self.con.write(data)

                sleep(0.01)
        except KeyboardInterrupt:
            self.active = False

class RXSerial(threading.Thread):
    def __init__(self, con):
        threading.Thread.__init__(self)
        self.con = con
        self.daemon = True
        self.active = True
        self.buff = queue.Queue()
    
    def run(self):
        self.active = True

        while self.active:
            try:
                data = self.con.read()
                if data:
                    print(data.decode('utf-8'), end='')
                sleep(0.01)
            except UnicodeDecodeError:
                pass
            except KeyboardInterrupt:
                self.active = False

def parse_args():
    parser = argparse.ArgumentParser('DCS BIOS F18 Simulator')
    parser.add_argument('serialdata', help='Binary file with recorded serial data')
    parser.add_argument('commport', help='Comm port that connects to the UFC')
    parser.add_argument('-b', '--baud', default=250000, help='Baud rate')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()

    con = serial.Serial(args.commport, baudrate=args.baud, timeout=0.001)

    tx_thread = TXSerial(con)
    rx_thread = RXSerial(con)

    tx_thread.start()
    rx_thread.start()

    data = ""
    with open(args.serialdata, 'rb') as fp:
        data = fp.read()
    
    # form data into sync sequences and records
    dataset = data.split(b"\x55\x55\x55\x55")

    # drop first row if empty
    if len(dataset[0]) == 0:
        dataset = dataset[1:]
    i = 0
    
    rx_data = ""
    sent = False
    while tx_thread.active and rx_thread.active:
        try:
            if i < len(dataset) - 1:
                start = time()
                tx_thread.buff.put(b"\x55\x55\x55\x55" + dataset[i])
                i += 1
                sleep_for = (1.0/30.0) - (time() - start)
                sleep(sleep_for)
            else:
                sleep(0.5)
            
        except KeyboardInterrupt:
            tx_thread.active = False
            rx_thread.active = False

            tx_thread.join()
            rx_thread.join()
            break

    con.close()

    fp.close()
