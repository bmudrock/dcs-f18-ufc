/*
 * fifo_uart.h
 *
 *  Created on: Sep 6, 2020
 *      Author: bradm
 */

#ifndef INC_FIFO_UART_H_
#define INC_FIFO_UART_H_

#include "main.h"
#include "fifo.h"

#define FIFO_SIZE 512

#define UART_TX_IDLE 0
#define UART_TX_IN_PROGRESS 1

#define UART_RX_IDLE 0
#define UART_RX_DATA_READY 1

#define UART_BLOCKING_TIMEOUT_MS 5

typedef struct {
  volatile uint32_t push, pop;
  uint32_t mask;
  uint8_t buf[FIFO_SIZE];
} FIFO;

typedef struct
{
  UART_HandleTypeDef *huart;
  volatile uint32_t rx_status;
  volatile uint32_t tx_status;
  FIFO tx_fifo;
  FIFO rx_fifo;
} FIFO_Uart;

void FIFO_Uart_Init(FIFO_Uart *fuart, UART_HandleTypeDef *huart);
void FIFO_Uart_Enable(FIFO_Uart *fuart);
void FIFO_Uart_Disable(FIFO_Uart *fuart);
uint32_t FIFO_Uart_Receive(FIFO_Uart *fuart, uint8_t *data, uint32_t len);
uint32_t FIFO_Uart_BlockingReceive(FIFO_Uart *fuart, uint8_t *data, uint32_t len);
uint32_t FIFO_Uart_Transmit(FIFO_Uart *fuart, uint8_t *data, uint32_t len);
void FIFO_Uart_ProcessData(FIFO_Uart *fuart);
uint32_t FIFO_Uart_DataPresent(FIFO_Uart *fuart);

#endif /* INC_FIFO_UART_H_ */
