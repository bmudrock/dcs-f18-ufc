/*
 * bool.h
 *
 *  Created on: Sep 6, 2020
 *      Author: bradm
 */

#ifndef INC_BOOL_H_
#define INC_BOOL_H_

#define FALSE 0
#define TRUE 1

#endif /* INC_BOOL_H_ */
