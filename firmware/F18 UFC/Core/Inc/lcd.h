/*
 * lcd.h
 *
 *  Created on: Jul 17, 2022
 *      Author: bradm
 */

#ifndef INC_LCD_H_
#define INC_LCD_H_

void lcd_conf_all(void);
void lcd_conf(uint32_t index);
void lcd_print(uint32_t index, uint8_t *message, uint32_t length);

#endif /* INC_LCD_H_ */
