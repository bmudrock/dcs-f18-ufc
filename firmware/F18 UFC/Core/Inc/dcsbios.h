/*
 * dcsbios.h
 *
 *  Created on: Jul 23, 2022
 *      Author: bradm
 */

#ifndef INC_DCSBIOS_H_
#define INC_DCSBIOS_H_

#include "main.h"

void dcsbios_parse_data(void);
void dcsbios_init(UART_HandleTypeDef *huart);
void dcsbios_uart_isr(UART_HandleTypeDef *huart);
void dcsbios_send(uint8_t *msg, uint8_t len);
uint32_t dcsbios_get_value(uint8_t *dst, uint32_t addr, uint32_t len, uint32_t dst_offset);

#endif /* INC_DCSBIOS_H_ */
