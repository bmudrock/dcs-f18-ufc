/********************************************************************
 *
 * Filename: fifo.h
 *
 * Author:   Brad Murdoch
 *
 * Description:
 *     Generic Fifo Code.
 *
 *     NOTE: The Size of the Fifo must be a power of 2
 *
 *     Example:
 *
 *     #include "fifo.h"
 *     FIFO_CREATE(f, int, 512);
 *
 *     FIFO_RESET(f);
 *     if (!FIFO_FULL(f))
 *         FIFO_PUSH(f, 10);
 *     if (!FIFO_EMPTY(f))
 *         int p = FIFO_POP(f);
 *
 ********************************************************************/


#ifndef FIFO_H
#define FIFO_H

#include <string.h>

#define FIFO_CREATE(name, type, size) \
static struct { \
    volatile uint32_t push, pop;  \
    uint32_t mask; \
    type buf[(size)]; \
} name = { \
    .mask = (size) - 1 \
}

#define FIFO_INIT(fifo, size) ((fifo).mask = (size) - 1)

#define FIFO_FULL(fifo)   ((((fifo).push + 1) & (fifo).mask) == (fifo).pop)
#define FIFO_EMPTY(fifo)  ((fifo).push == (fifo).pop)

#define FIFO_CONT_SPACE(fifo)   \
({int end = (fifo).mask + 1 - (fifo).push; \
  int space = ((fifo).pop - (fifo).push - 1) & (fifo).mask;  \
  end < space ? end : space; \
})

#define FIFO_SPACE(fifo) (((fifo).pop - (fifo).push - 1) & (fifo).mask)

#define FIFO_CONT_DATA(fifo) \
({ \
    int end = (fifo).mask + 1 - (fifo).pop; \
    int data = ((fifo).push - (fifo).pop) & (fifo).mask;  \
    end < data ? end : data; \
})

#define FIFO_DATA(fifo) (((fifo).push - (fifo).pop) & (fifo).mask)

#define FIFO_PUSH(fifo, p) \
({ \
    (fifo).buf[(fifo).push] = p; \
    (fifo).push++; \
    (fifo).push &= (fifo).mask; \
})

#define FIFO_BULK_PUSH(fifo, data, count) \
({ \
    size_t s = FIFO_CONT_SPACE(fifo); \
    if (s > (count)) s = (count); \
    memcpy(&fifo.buf[(fifo).push], (data), sizeof((fifo).buf[0])*s); \
    (fifo).push += s; \
    (fifo).push &= (fifo).mask; \
    size_t s2 = (count) - s; \
    if (s2 > FIFO_SPACE(fifo)) s2 = FIFO_SPACE(fifo); \
    memcpy(&(fifo).buf[(fifo).push], &(data)[s], sizeof((fifo).buf[0])*s2); \
    (fifo).push += s2; \
    (fifo).push &= (fifo).mask; \
    (s + s2); \
})

#define FIFO_POP(fifo) \
({ \
    typeof((fifo).buf[0]) p  = (fifo).buf[(fifo).pop]; \
    (fifo).pop++; \
    (fifo).pop &= (fifo).mask; \
    p; \
})

/* NOTE: dest must be an pointer */
#define FIFO_BULK_POP(fifo, dst, count) \
({ \
    size_t s1 = FIFO_CONT_DATA(fifo); \
    if (s1 > (count)) s1 = (count); \
    memcpy((dst), &(fifo).buf[(fifo).pop], sizeof((fifo).buf[0])*s1); \
    (fifo).pop += s1; \
    (fifo).pop &= (fifo).mask; \
    size_t s2 = (count) - s1; \
    if (s2 > FIFO_DATA((fifo))) s2 = FIFO_DATA((fifo)); \
    memcpy(&(dst)[s1], &(fifo).buf[(fifo).pop], sizeof((fifo).buf[0])*s2); \
    (fifo).pop += s2; \
    (fifo).pop &= (fifo).mask; \
    (s1 + s2); \
})

#define FIFO_PEEK(fifo) \
({ \
    fifo.buf[(fifo).pop]; \
})


#define FIFO_RESET(fifo) \
({ \
    fifo.pop = fifo.push = 0; \
})

//Pop off and throw away count elements from the FIFO
#define FIFO_TOSS(fifo, count) \
({ \
    int s = count; \
    if (s > FIFO_DATA(fifo)) s = FIFO_DATA(fifo); \
    (fifo).pop += s; \
    (fifo).pop &= (fifo).mask; \
})

#endif

