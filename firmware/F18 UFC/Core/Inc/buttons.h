/*
 * buttons.h
 *
 *  Created on: Jul 22, 2022
 *      Author: bradm
 */

#ifndef INC_BUTTONS_H_
#define INC_BUTTONS_H_

#include <stdint.h>

#define BUTTON_DOWN   GPIO_PIN_SET
#define BUTTON_UP     GPIO_PIN_RESET

#define BUTTON_1     0x00000001
#define BUTTON_2     0x00000002
#define BUTTON_3     0x00000004
#define BUTTON_4     0x00000008
#define BUTTON_5     0x00000010
#define BUTTON_6     0x00000020
#define BUTTON_7     0x00000040
#define BUTTON_8     0x00000080
#define BUTTON_9     0x00000100
#define BUTTON_0     0x00000200
#define BUTTON_CLR   0x00000400
#define BUTTON_ENT   0x00000800
#define BUTTON_CIR1  0x00001000
#define BUTTON_CIR2  0x00002000
#define BUTTON_CIR3  0x00004000
#define BUTTON_CIR4  0x00008000
#define BUTTON_CIR5  0x00010000
#define BUTTON_AP    0x00020000
#define BUTTON_IFF   0x00040000
#define BUTTON_TCN   0x00080000
#define BUTTON_ILS   0x00100000
#define BUTTON_DL    0x00200000
#define BUTTON_BCN   0x00400000
#define BUTTON_ONOFF 0x00800000

#define MAX_ID_LEN 50

typedef struct {
  uint8_t dcsid[MAX_ID_LEN];
  uint32_t prev_value;
  uint32_t value;
} button_t;

typedef struct {
  int values[5][5];
} buttons_t;

void buttons_init(void);
void buttons_process_updates(void);


#endif /* INC_BUTTONS_H_ */
