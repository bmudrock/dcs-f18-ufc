/*
 * dcsbios.c
 *
 *  Created on: Jul 23, 2022
 *      Author: bradm
 */
#include <stdio.h>
#include "dcsbios.h"
#include "fifo_uart.h"

#define STATE_LOOKING_FOR_SYNC 0
#define STATE_READING_ADDRESS 1
#define STATE_READING_LENGTH 2
#define STATE_READING_DATA 3

#define SYNC_SEQ_VAL 0x55
#define SYNC_SEQ_LEN 4

#define MMAP_LEN 42

FIFO_Uart dcsbios_fifo;

typedef struct
{
    uint16_t address;
    uint16_t length;
} dcsbios_record_t;


/*
 * The memory map DCSBIOS creates is done via records. A given record may say to write 0x1234 with 20bytes of data
 * that memory could contain 4 displays to update. 
 * Instead of copying the entire memory map (65535 values), we only care about 42bytes (the displays of the UFC)
 * These displays start of 0x7428 and end at 0x7452.
 */
const uint32_t mmap_offset = 0x7428;
static volatile uint8_t mmap[MMAP_LEN];

/*
 * @brief Setup the DCSBIOS fifo, ensure we wait for a sync seq
 */
void dcsbios_init(UART_HandleTypeDef *huart)
{
    FIFO_Uart_Init(&dcsbios_fifo, huart);
    FIFO_Uart_Enable(&dcsbios_fifo);

    for(int i=0;i<MMAP_LEN;i++)
        mmap[i] = '|';
}

uint32_t dcsbios_get_value(uint8_t *dst, uint32_t addr, uint32_t len, uint32_t dst_offset)
{
    int32_t adjusted_addr = addr - mmap_offset;

    if(adjusted_addr >= 0 && adjusted_addr < MMAP_LEN)
    {
        //printf("Getting %ld bytes from 0x%04lX [%ld]\r\n", len, addr, adjusted_addr);
        for(int i=0;i<len;i++)
            dst[i + dst_offset] = mmap[adjusted_addr + i];
        return 1;
    }
    printf("Bad get\r\n");
    return 0;
}

void dcsbios_parse_data(void)
{    
    static volatile uint32_t consecutive_sync_bytes = 0;
    static volatile uint32_t state = STATE_LOOKING_FOR_SYNC;
    static volatile uint32_t bytes_read = 0;
    static volatile dcsbios_record_t record;

    int32_t adjusted_addr;
    uint8_t byte;

    while(!FIFO_EMPTY(dcsbios_fifo.rx_fifo))
    {
        byte = FIFO_POP(dcsbios_fifo.rx_fifo);
        bytes_read += 1;

        // always track how many seq bytes we get, reset to reading address if we get enough
        if(byte == SYNC_SEQ_VAL)
            consecutive_sync_bytes += 1;
        else
            consecutive_sync_bytes = 0;
        
        if(consecutive_sync_bytes == SYNC_SEQ_LEN)
        {
            bytes_read = 0;
            record.address = 0;
            state = STATE_READING_ADDRESS;
            continue;
        }

        // no need to catch looking for sync, it is handled automatically        
        switch(state)
        {
            case STATE_READING_ADDRESS:
            {
                record.address |= (byte << ((bytes_read-1) * 8));
                if(bytes_read == 2)
                {
                    state = STATE_READING_LENGTH;
                    record.length = 0;
                    bytes_read = 0;
                }
                break;
            }
            case STATE_READING_LENGTH:
            {
                record.length |= (byte << ((bytes_read-1) * 8));
                if(bytes_read == 2)
                {
                    state = STATE_READING_DATA;
                    bytes_read = 0;
                }
                break;
            }
            case STATE_READING_DATA:
            {
                // if new byte is within our memory map, record it
                adjusted_addr = record.address - mmap_offset + (bytes_read - 1);               
                if(adjusted_addr >= 0 && adjusted_addr < MMAP_LEN)
                    mmap[adjusted_addr] = byte;

                if(bytes_read == record.length)
                {
                    state = STATE_READING_ADDRESS;
                    record.address = 0;
                    bytes_read = 0;
                }
                break;
            }
        }
    }
}

void dcsbios_send(uint8_t *msg, uint8_t len)
{
    FIFO_Uart_Transmit(&dcsbios_fifo, msg, len);
}

void dcsbios_uart_isr(UART_HandleTypeDef *huart)
{
    // process the UART interrupt and either rx data into rx_fifo or tx data from tx_fifo
    FIFO_Uart_ProcessData(&dcsbios_fifo);
    
}