/*
 * buttons.c
 *
 *  Created on: Jul 22, 2022
 *      Author: bradm
 */
#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "buttons.h"
#include "dcsbios.h"

#define BUTTON_DEBOUNCE_TIME 2

// [row][col]
button_t buttons[5][5] = {
  { // row 1
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_1\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_2\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_3\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_OS1\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_OS2\0"}
  },
  { // row 2
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_4\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_5\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_6\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_OS3\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_BCN\0"}
  },
  { // row 3
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_7\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_8\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_9\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_OS4\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_ONOFF\0"}
  },
  { // row 4
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_CLR\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_0\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_ENT\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_OS5\0"},
    {.value=0, .dcsid="\0"}
  },
  { // row 5
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_AP\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_IFF\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_TCN\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_ILS\0"}, 
    {.prev_value=BUTTON_UP, .value=BUTTON_UP, .dcsid="UFC_DL\0"}
  }
};

typedef struct
{
  GPIO_TypeDef *port;
  uint32_t pin;
} col_row_t;

col_row_t cols[] = {
    [0] = {.port=COL1_GPIO_Port, .pin=COL1_Pin},
    [1] = {.port=COL2_GPIO_Port, .pin=COL2_Pin},
    [2] = {.port=COL3_GPIO_Port, .pin=COL3_Pin},
    [3] = {.port=COL4_GPIO_Port, .pin=COL4_Pin},
    [4] = {.port=COL5_GPIO_Port, .pin=COL5_Pin}
};

col_row_t rows[] = {
    [0] = {.port=ROW1_GPIO_Port, .pin=ROW1_Pin},
    [1] = {.port=ROW2_GPIO_Port, .pin=ROW2_Pin},
    [2] = {.port=ROW3_GPIO_Port, .pin=ROW3_Pin},
    [3] = {.port=ROW4_GPIO_Port, .pin=ROW4_Pin},
    [4] = {.port=ROW5_GPIO_Port, .pin=ROW5_Pin}
};

void scan(void);

void buttons_init(void)
{
  for(int col=0;col<5;col++)
      HAL_GPIO_WritePin(cols[col].port, cols[col].pin, GPIO_PIN_RESET);
  
  for(int row=0;row<5;row++)
  {
    for(int col=0;col<5;col++)
    {
      buttons[row][col].value = BUTTON_UP;
      buttons[row][col].prev_value = BUTTON_UP;
    }
  }
}

void buttons_process_updates(void)
{
  scan();
  
  for(int row=0;row<5;row++)
  {
    for(int col=0;col<5;col++)
    {
      if(buttons[row][col].value != buttons[row][col].prev_value)
      {
        printf("%s %ld\r\n", buttons[row][col].dcsid, buttons[row][col].value);

        buttons[row][col].prev_value = buttons[row][col].value;
      }
    }
  }
}

void scan(void)
{
  // ensure all columns are off
  for(int col=0;col<5;col++)
    HAL_GPIO_WritePin(cols[col].port, cols[col].pin, GPIO_PIN_RESET);

  for(int col=0;col<5;col++)
  {
    HAL_GPIO_WritePin(cols[col].port, cols[col].pin, GPIO_PIN_SET);
    HAL_Delay(BUTTON_DEBOUNCE_TIME);

    for(int row=0;row<5;row++)
      buttons[row][col].value = HAL_GPIO_ReadPin(rows[row].port, rows[row].pin);

    HAL_GPIO_WritePin(cols[col].port, cols[col].pin, GPIO_PIN_RESET);
  }
}
