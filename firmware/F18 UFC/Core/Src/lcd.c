/*
 * lcd.c
 *
 *  Created on: Jul 17, 2022
 *      Author: bradm
 */
#include <stdio.h>
#include "main.h"

#define LCD_CLEAR_DISPLAY           0x01
#define LCD_RETURN_HOME             0x02
#define LCD_DECREMENT_CURSOR        0x04
#define LCD_INCREMENT_CURSOR        0x06
#define LCD_SHIFT_LEFT              0x05
#define LCD_SHIFT_RIGHT             0x07
#define LCD_DISPLAY_OFF_CURSOR_OFF  0x08
#define LCD_DISPLAY_OFF_CURSOR_ON   0x0A
#define LCD_DISPLAY_ON_CURSOR_OFF   0x0C
#define LCD_DISPLAY_ON_CURSOR_ON    0x0E
#define LCD_DISPLAY_ON_CURSOR_BLINK 0x0B
#define LCD_SHIFT_CURSOR_POS_LEFT   0x10
#define LCD_SHIFT_CURSOR_POS_RIGHT  0x14
#define LCD_SHIFT_DISP_LEFT         0x18
#define LCD_SHIFT_DISP_RIGHT        0x1C
#define LCD_GOTO_LINE_1             0x80
#define LCD_GOTO_LINE_2             0xC0
#define LCD_8BIT_MODE               0x30

#define LCD_READ  GPIO_PIN_SET
#define LCD_WRITE GPIO_PIN_RESET

#define LCD_DATA_REGISTER GPIO_PIN_SET
#define LCD_CMD_REGISTER  GPIO_PIN_RESET

#define LCD_DELAY 2

void lcd_write_cmd(uint32_t index, uint8_t command);
void lcd_write_data(uint32_t index, uint8_t *message, uint32_t length);
void lcd_reset_enable(uint32_t index);
void lcd_set_enable(uint32_t index);
uint32_t lcd_index_check(uint32_t index, uint32_t length);
void lcd_set_byte(uint8_t byte);
uint8_t lcd_translate(uint8_t byte);

uint32_t lcd_index_check(uint32_t index, uint32_t length)
{
  if(index == 0)
    return (length <= 16);
  return (length <= 8);
}

uint8_t lcd_translate(uint8_t byte)
{
  if(byte == 0x40)
    return 0xDF;
  return byte;
}

void lcd_set_enable(uint32_t index)
{
  switch(index)
  {
    case 0:
      HAL_GPIO_WritePin(E0_GPIO_Port, E0_Pin, GPIO_PIN_RESET);
      break;
    case 1:
      HAL_GPIO_WritePin(E1_GPIO_Port, E1_Pin, GPIO_PIN_RESET);
      break;
    case 2:
      HAL_GPIO_WritePin(E2_GPIO_Port, E2_Pin, GPIO_PIN_RESET);
      break;
    case 3:
      HAL_GPIO_WritePin(E3_GPIO_Port, E3_Pin, GPIO_PIN_RESET);
      break;
    case 4:
      HAL_GPIO_WritePin(E4_GPIO_Port, E4_Pin, GPIO_PIN_RESET);
      break;
    case 5:
      HAL_GPIO_WritePin(E5_GPIO_Port, E5_Pin, GPIO_PIN_RESET);
      break;
  }
}

void lcd_reset_enable(uint32_t index)
{
  switch(index)
  {
    case 0:
      HAL_GPIO_WritePin(E0_GPIO_Port, E0_Pin, GPIO_PIN_SET);
      break;
    case 1:
      HAL_GPIO_WritePin(E1_GPIO_Port, E1_Pin, GPIO_PIN_SET);
      break;
    case 2:
      HAL_GPIO_WritePin(E2_GPIO_Port, E2_Pin, GPIO_PIN_SET);
      break;
    case 3:
      HAL_GPIO_WritePin(E3_GPIO_Port, E3_Pin, GPIO_PIN_SET);
      break;
    case 4:
      HAL_GPIO_WritePin(E4_GPIO_Port, E4_Pin, GPIO_PIN_SET);
      break;
    case 5:
      HAL_GPIO_WritePin(E5_GPIO_Port, E5_Pin, GPIO_PIN_SET);
      break;
    }
}

void lcd_set_byte(uint8_t byte)
{
  HAL_GPIO_WritePin(DB0_GPIO_Port, DB0_Pin, byte & 0x01);
  HAL_GPIO_WritePin(DB1_GPIO_Port, DB1_Pin, byte & 0x02);
  HAL_GPIO_WritePin(DB2_GPIO_Port, DB2_Pin, byte & 0x04);
  HAL_GPIO_WritePin(DB3_GPIO_Port, DB3_Pin, byte & 0x08);
  HAL_GPIO_WritePin(DB4_GPIO_Port, DB4_Pin, byte & 0x10);
  HAL_GPIO_WritePin(DB5_GPIO_Port, DB5_Pin, byte & 0x20);
  HAL_GPIO_WritePin(DB6_GPIO_Port, DB6_Pin, byte & 0x40);
  HAL_GPIO_WritePin(DB7_GPIO_Port, DB7_Pin, byte & 0x80);
}

/*
 * requires enable pin to be set prior
 */
void lcd_write_data(uint32_t index, uint8_t *message, uint32_t length)
{
  if (index == 0 && length > 16)
    length = 16;
  if (index > 0 && length > 8)
    length = 8;

  for(int byte=0;byte<length;byte++)
  {
    HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, LCD_DATA_REGISTER);
    HAL_GPIO_WritePin(RW_GPIO_Port, RW_Pin, LCD_WRITE);
    lcd_set_byte(lcd_translate(message[byte]));
    lcd_set_enable(index);
    HAL_Delay(LCD_DELAY);
    lcd_reset_enable(index);
    if (byte == 7)
      lcd_write_cmd(index, 0xC0);
  }
}

/*
 * requires enable pin to be set prior
 */
void lcd_write_cmd(uint32_t index, uint8_t command)
{
  HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, LCD_CMD_REGISTER);
  HAL_GPIO_WritePin(RW_GPIO_Port, RW_Pin, LCD_WRITE);

  lcd_set_byte(command);

  lcd_set_enable(index);
  HAL_Delay(LCD_DELAY);
  lcd_reset_enable(index);
}

void lcd_conf(uint32_t index)
{
  lcd_write_cmd(index, 0x38);
  lcd_write_cmd(index, LCD_INCREMENT_CURSOR);
  lcd_write_cmd(index, LCD_DISPLAY_ON_CURSOR_OFF);
  lcd_write_cmd(index, LCD_CLEAR_DISPLAY);
}

/*
 * run for all 6 LCDs
 */
void lcd_conf_all(void)
{
  // data sheet requests 40msec from startup
  for(uint32_t i=0;i<6;i++)
    lcd_reset_enable(i);

  HAL_Delay(100);
  for(uint32_t i=0;i<6;i++)
    lcd_conf(i);
}

void lcd_print(uint32_t index, uint8_t *message, uint32_t length)
{
  lcd_write_cmd(index, LCD_CLEAR_DISPLAY);
  lcd_write_data(index, message, length);

  printf("%ld - [", index);
  for(int i=0;i<length;i++)
    printf("%c", message[i]);
  printf("]\t");
  for(int i=0;i<length;i++)
    printf("0x%02X ", message[i]);
  
  printf("\r\n");
}

