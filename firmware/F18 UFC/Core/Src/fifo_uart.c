/*
 * fifo_uart.c
 *
 *  Created on: Sep 6, 2020
 *      Author: bradm
 */


#include <stdint.h>
#include "fifo_uart.h"
#include "fifo.h"
#include "bool.h"


void FIFO_Uart_Init(FIFO_Uart *fuart, UART_HandleTypeDef *huart)
{
  /** Init the FIFOs and initial status */
  fuart->huart = huart;
  FIFO_INIT(fuart->rx_fifo, FIFO_SIZE);
  FIFO_INIT(fuart->tx_fifo, FIFO_SIZE);
  fuart->rx_status = UART_RX_IDLE;
  fuart->tx_status = UART_TX_IDLE;

  // enable transmitter and receiver
  SET_BIT(fuart->huart->Instance->CR1, USART_CR1_RE);
  SET_BIT(fuart->huart->Instance->CR1, USART_CR1_TE);
}

void FIFO_Uart_Enable(FIFO_Uart *fuart)
{
  /** Enable the UART for reception */
  SET_BIT(fuart->huart->Instance->CR1, USART_CR1_RXNEIE);
}

void FIFO_Uart_Disable(FIFO_Uart *fuart)
{
  /** Enable the UART for reception */
  CLEAR_BIT(fuart->huart->Instance->CR1, USART_CR1_RXNEIE);
}

uint32_t FIFO_Uart_Receive(FIFO_Uart *fuart, uint8_t *data, uint32_t len)
{
  uint32_t read = 0;

  if (FIFO_EMPTY(fuart->rx_fifo))
    return 0;

  while(!FIFO_EMPTY(fuart->rx_fifo) && read < len)
  {
    data[read] = FIFO_POP(fuart->rx_fifo);
    read += 1;
  }
  return read;
}

uint32_t FIFO_Uart_BlockingReceive(FIFO_Uart *fuart, uint8_t *data, uint32_t len)
{
  uint32_t read = 0;
  uint32_t timeout = HAL_GetTick() + UART_BLOCKING_TIMEOUT_MS;

  while(read < len)
  {
    while(FIFO_EMPTY(fuart->rx_fifo))
    {
      if(HAL_GetTick() > timeout)
        return 0;
    }
    
    data[read] = FIFO_POP(fuart->rx_fifo);
    read += 1;
  }
  return read;
}

uint32_t FIFO_Uart_Transmit(FIFO_Uart *fuart, uint8_t *data, uint32_t len)
{
  uint32_t wrote;

  for(wrote=0; wrote<len; wrote++)
  {
    if (!FIFO_FULL(fuart->tx_fifo))
      FIFO_PUSH(fuart->tx_fifo, data[wrote]);
  }

  // begin the transmission
  fuart->huart->Instance->TDR = FIFO_POP(fuart->tx_fifo);

  // enable the TX buffer empty interrupt
  SET_BIT(fuart->huart->Instance->CR1, USART_CR1_TCIE);
  return wrote;
}

void FIFO_Uart_ProcessData(FIFO_Uart *fuart)
{
  /** Handle the RX and TX Interrupt Sequences */
  // if data is in the RX buffer
  if (fuart->huart->Instance->ISR & USART_ISR_RXNE)
  {
    if (!FIFO_FULL(fuart->rx_fifo))
      FIFO_PUSH(fuart->rx_fifo, fuart->huart->Instance->RDR);
    else
      SET_BIT(fuart->huart->Instance->RQR, USART_RQR_RXFRQ);
  }

  // if the tx buffer has room, and we have bytes left to send
  if (fuart->huart->Instance->ISR & USART_ISR_TXE)
  {
    if (!FIFO_EMPTY(fuart->tx_fifo))
    {
      fuart->huart->Instance->TDR = FIFO_POP(fuart->tx_fifo);
      fuart->tx_status = UART_TX_IN_PROGRESS;
    }
    else
    {
      // disable tx buff empty interrupt
      CLEAR_BIT(fuart->huart->Instance->CR1, USART_CR1_TXEIE);

      // enable tx complete interrupt
      SET_BIT(fuart->huart->Instance->CR1, USART_CR1_TCIE);
    }
  }

  // if the transmission is complete
  if (fuart->huart->Instance->ISR & USART_ISR_TC)
  {
    // disable and clear the transmission complete interrupt
    CLEAR_BIT(fuart->huart->Instance->CR1, USART_CR1_TCIE);
    SET_BIT(fuart->huart->Instance->ICR, USART_ICR_TCCF);
    fuart->tx_status = UART_TX_IDLE;
  }
}

uint32_t FIFO_Uart_DataPresent(FIFO_Uart *fuart)
{
  /** Is their any data to receive */
  if (!FIFO_EMPTY(fuart->rx_fifo))
    return TRUE;
  return FALSE;
}
